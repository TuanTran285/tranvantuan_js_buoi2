const btn = document.querySelector("#submit");

function tong2So() {
  const input = document.querySelector("#nhap-so").value * 1;
  const message = document.querySelector(".form-total");
  if (input) {
    const donVi = input % 10
    const chuc = Math.floor(input / 10)
    const tong2KySo = donVi + chuc
    message.innerText = tong2KySo
    message.style.display = 'flex'
  }
}

btn.addEventListener("click", function (e) {
  e.preventDefault();
  tong2So();
});
