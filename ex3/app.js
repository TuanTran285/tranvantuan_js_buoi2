const btn = document.querySelector("#submit")

function ChangeMoney() {
    const nhapSoTien = document.querySelector("#nhap-so-tien").value * 1
    const message = document.querySelector(".form-total")
    const usdDefault = 23.500
    let result = 0
    if(nhapSoTien) {
        result = nhapSoTien * usdDefault
        message.innerText = result.toFixed(3) + " vnd"
        message.style.display = "flex"
    }else {
        confirm("Nhập số tiền muốn quy đổi")
    }

    
}



btn.addEventListener("click", function(e) {
    e.preventDefault()
    ChangeMoney()
})