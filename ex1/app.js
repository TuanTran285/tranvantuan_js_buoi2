const btn = document.querySelector("#submit")
function tinhLuong() {
    const tienLuong = document.querySelector("#tien-luong").value * 1
    const soNgayLam = document.querySelector("#so-ngay-lam").value * 1
    const message = document.querySelector(".form-total")
    const tongTien = tienLuong * soNgayLam
    message.innerText = tongTien
    message.style.display = "block"
}
function keyCode() {
    const inputsEl = document.querySelectorAll(".form-input")
    inputsEl.forEach(input => {
        input.addEventListener("keydown", function(e) {
            if(e.code == "Enter") {
                tinhLuong()
            }
        })
    })
    document.body.addEventListener("keydown", function(e) {
        if(e.code == "Escape") {
            alert("bạn có muốn thoát khỏi đây không?")
        }
    })

}

keyCode()

btn.addEventListener("click", function() {
    tinhLuong()
})
