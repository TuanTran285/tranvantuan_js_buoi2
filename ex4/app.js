const btn = document.querySelector("#submit")



function dienTichHCN() {
    const chieuDai = document.querySelector("#chieu-dai").value * 1
    const chieuRong = document.querySelector("#chieu-rong").value * 1
    const message = document.querySelector("#dien-tich")
    const dienTich = chieuDai * chieuRong
    if(dienTich) {
        message.innerText = `Diện tích: ${dienTich}`
        message.style.display = "flex"
    }

}


function chuViHCN() {
    const chieuDai = document.querySelector("#chieu-dai").value * 1
    const chieuRong = document.querySelector("#chieu-rong").value * 1
    const message = document.querySelector("#chu-vi")
    const chuVi = (chieuDai + chieuRong) * 2
    if(chuVi) {
        message.innerText = `Chu Vi: ${chuVi}`
        message.style.display = "flex"
    }
}

btn.addEventListener("click", function(e) {
    e.preventDefault()
    dienTichHCN()
    chuViHCN()
})