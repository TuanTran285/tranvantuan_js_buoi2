const btn = document.querySelector("#submit")

function tinhTrungBinh() {
    const inputsEl = document.querySelectorAll("#nhap-so")
    const message = document.querySelector(".form-total")
    let total = 0
    inputsEl.forEach(input => {
        const inputValue = parseFloat(input.value)
        if(inputValue >= 0) {
            total += inputValue / inputsEl.length
            message.innerText = total.toFixed(2)
            message.style.display = "flex"
        }else {
            message.style.display = "none"
            alert("Nhập đầy đủ thông tin")
        }
    })
}


btn.addEventListener("click", function(e) {
    e.preventDefault()
    tinhTrungBinh()
})
